<?php
/**
 * Created by Jorge Sivil <jorge.sivil@gmail.com>
 */

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Item implements \JsonSerializable {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Field(type="string")
     */
    private $imageId;

    /**
     * @MongoDB\Field(type="string")
     */
    private $description;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getImageId() {
        return $this->imageId;
    }

    /**
     * @param mixed $imageId
     */
    public function setImageId($imageId): void {
        $this->imageId = $imageId;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void {
        $this->description = $description;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'img_id' => $this->getImageId(),
            'description' => $this->getDescription(),
        ];
    }
}