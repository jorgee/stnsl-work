<?php
/**
 * Created by Jorge Sivil <jorge.sivil@gmail.com>
 */
namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(collection="itemslists")
 */
class ItemsList implements \JsonSerializable {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\EmbedMany(targetDocument="Item")
     */
    private $items = [];

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * @param mixed $items
     */
    public function setItems($items): void {
        $this->items = $items;
    }

    public function jsonSerialize() {
        return [
            'id' => $this->getId(),
            'items' => iterator_to_array($this->getItems()),
        ];
    }

}

