<?php
/**
 * Created by Jorge Sivil <jorge.sivil@gmail.com>
 */

namespace App\Controller;

use App\Document\Item;
use App\Document\ItemsList;
use App\Services\FileUploader\FileUploader;
use Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Doctrine\ODM\MongoDB\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ItemsListController extends Controller {

    /**
     * Gets an Items List
     * @param $listId
     *ever
     * @return JsonResponse
     */
    public function getList($listId) {
        /** @var ItemsList $itemsList */
        $itemsList = $this->findListOrThrowError($listId);

        return new JsonResponse($itemsList);
    }

    /**
     * Gets the first list ever.
     * @return JsonResponse
     */
    public function getFirstList() {
        $itemsListRepository = $this->getItemsListRepository();
        /** @var ItemsList $itemsList */
        $itemsLists = $itemsListRepository->findBy([], null, 1);

        if (!isset($itemsLists[0])) {
            throw new HttpException(404, 'There aren\'t any lists available');
        }

        $itemsList = $itemsLists[0];
        return new JsonResponse($itemsList);
    }

    /**
     * Adds an item to an existing list.
     * @param $listId
     *
     * @return JsonResponse|Response
     */
    public function addItem($listId) {
        $request = Request::createFromGlobals();
        $itemDescription = $request->request->get('description');

        if (!$itemDescription) {
            throw new HttpException(400, 'Missing \'description\' field.');
        }
        if (strlen($itemDescription) > 300) {
            throw new HttpException(400, 'The \'description\' field cannot exceed 300 chars.');
        }

        /** @var FileUploader $fileUploader */
        $fileUploader = $this->get('item_image_uploader');
        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        $this->checkImageSizeOrThrowException($file->getPathname());
        $this->checkImageTypeOrThrowException($file->getPathname());

        $fileName = $fileUploader->upload($file);
        $itemImagePath = $fileName;

        /** @var ItemsList $itemsList */
        $itemsList = $this->findListOrThrowError($listId);
        $newItem = new Item();
        $newItem->setImageId($itemImagePath);
        $newItem->setDescription($itemDescription);

        /** @var PersistentCollection $items */
        $items = $itemsList->getItems();
        $items->add($newItem);

        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->flush($itemsList);

        $outputHtml = $request->request->get('outputHtml',false);
        if ($outputHtml) {
            return $this->render('items-list/item.html.twig', [
                'itemId' => $newItem->getId(),
                'itemImageId' => $newItem->getImageId(),
                'itemDescription' => $newItem->getDescription()
            ]);
        }
        return new JsonResponse($itemsList);
    }

    /**
     * @param $listId
     * @param $itemId
     *
     * @return JsonResponse|Response
     */
    public function editItem($listId, $itemId) {
        $itemsList = $this->findListWithItemOrThrowError($listId, $itemId);

        $request = Request::createFromGlobals();
        $itemDescription = $request->request->get('description');
        if (strlen($itemDescription) > 300) {
            throw new HttpException(400, 'The \'description\' field cannot exceed 300 chars.');
        }

        /** @var UploadedFile $file */
        $file = $request->files->get('image');
        $itemImageName = null;

        if ($file) {
            $this->checkImageSizeOrThrowException($file->getPathname());
            $this->checkImageTypeOrThrowException($file->getPathname());

            if ($file) {
                /** @var FileUploader $fileUploader */
                $fileUploader = $this->get('item_image_uploader');
                $fileName = $fileUploader->upload($file);
                $itemImageName = $fileName;
            }
        }

        $listItem = null;
        /** @var Item $item */
        foreach ($itemsList->getItems() as $item) {
            if ($item->getId() === $itemId) {
                $listItem = $item;
                break;
            }
        }

        if ($itemDescription !== null) {
            $listItem->setDescription($itemDescription);
        }
        if ($itemImageName !== null) {
            $listItem->setImageId($itemImageName);
        }

        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->flush($itemsList);
        $outputHtml = $request->request->get('outputHtml',false);
        if ($outputHtml) {
            return $this->render('items-list/item.html.twig', [
                'itemId' => $listItem->getId(),
                'itemImageId' => $listItem->getImageId(),
                'itemDescription' => $listItem->getDescription()
            ]);
        }
        return new JsonResponse($itemsList);
    }

    /**
     * @param $listId
     * @param $itemId
     *
     * @return JsonResponse
     */
    public function deleteItem($listId, $itemId) {
        $itemsList = $this->findListWithItemOrThrowError($listId, $itemId);
        /** @var PersistentCollection $itemsListIterator */
        $itemsListIterator = $itemsList->getItems();
        /** @var Item $item */
        foreach ($itemsListIterator as $key => $item) {
            if ($item->getId() === $itemId) {
                $itemsListIterator->remove($key);
                /** @var FileUploader $fileUploader */
                $fileUploader = $this->get('item_image_uploader');
                @unlink($fileUploader->getTargetDirectory() . '/' . $item->getImageId());
                /** @var DocumentManager $dm */
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->flush();
            }
        }

        return new JsonResponse($itemsList);
    }

    /**
     * Reorders the Items of an Items List.
     * Done with a comma-separated list of Items IDs. Non-existent IDs will not result in unexpected behaviour.
     * Other option would involve a PUT endpoint for updating the entire list, but then arbitrary things might be done there.
     * @param $listId
     *
     * @return JsonResponse
     */
    public function reorderItems($listId) {
        /** @var ItemsList $itemsList */
        $itemsList = $this->findListOrThrowError($listId);

        $request = Request::createFromGlobals();
        $orderParameter = $request->request->get('order');
        if (!$orderParameter) {
            throw new HttpException(400, 'Missing \'order\' field.');
        }

        $newOrder = explode(',', $orderParameter);

        $itemsDictionary = [];
        $itemsNewOrder = [];

        /** @var Item $item */
        foreach ($itemsList->getItems() as $item) {
            $itemsDictionary[$item->getId()] = $item;
        }

        foreach ($newOrder as $itemId) {
            if (isset($itemsDictionary[$itemId])) {
                $itemsNewOrder[] = $itemsDictionary[$itemId];
                unset($itemsDictionary[$itemId]);
            }
        }

        // Just in case, add remaining items that do not appear in the 'order' parameters but they exist.
        // This way, we do not loose items if less IDs than actual Items are sent.
        foreach ($itemsDictionary as $item) {
            $itemsNewOrder[] = $item;
        }

        $itemsList->setItems($itemsNewOrder);

        /** @var DocumentManager $dm */
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->flush($itemsList);

        return new JsonResponse($itemsList);
    }

    /**
     * @return DocumentRepository
     */
    protected function getItemsListRepository() : DocumentRepository {
        /** @var DocumentRepository $itemsListRepository */
        $itemsListRepository = $this->get('doctrine_mongodb')->getRepository('App:ItemsList');
        return $itemsListRepository;
    }

    /**
     * @param $listId
     * @param $itemId
     *
     * @return ItemsList|null|array|object
     */
    protected function findListWithItemOrThrowError($listId, $itemId) {
        $itemsListRepository = $this->getItemsListRepository();
        try {
            /** @var Cursor $itemsLists */
            $itemsLists = $itemsListRepository->createQueryBuilder()
                ->field('id')
                ->equals(new \MongoId($listId))
                ->field('items.id')
                ->equals(new \MongoId($itemId))
                ->getQuery()
                ->execute();
        } catch (\Exception $e) {
            throw new HttpException(500, 'Error while trying to fetch Item List.');
        }

        /** @var ItemsList|null $itemsList */
        $itemsList = $itemsLists->getSingleResult();
        if (!$itemsList) {
            throw new HttpException(404,
                sprintf('Could not find any Item List with ID %s having an Item with ID %s.', $listId, $itemId));
        }

        return $itemsList;
    }

    /**
     * @param $listId
     *
     * @return ItemsList|null|object
     */
    protected function findListOrThrowError($listId) {
        $itemsListRepository = $this->getItemsListRepository();
        /** @var ItemsList $itemsList */
        try {
            $itemsList = $itemsListRepository->find($listId);
        } catch (\Exception $e) {
            throw new HttpException(500, 'Error while trying to fetch Item List.');
        }

        if (!$itemsList) {
            throw new HttpException(404, sprintf('Could not find any Item List with ID %s.', $listId));
        }

        return $itemsList;
    }

    /**
     * @param $imagePath
     *
     * @return bool
     */
    protected function imageTypeIsAllowed($imagePath) {
        $imageType = exif_imagetype($imagePath);
        return $imageType === IMAGETYPE_GIF || $imageType === IMAGETYPE_JPEG || $imageType === IMAGETYPE_PNG;
    }

    /**
     * @param $imagePath
     *
     * @return bool
     */
    protected function imageSizeIsAllowed($imagePath) {
        $imageInfo = getimagesize($imagePath);
        $imageWidth = $imageInfo[0];
        $imageHeight = $imageInfo[1];
        return $imageWidth === 320 && $imageHeight === 320;
    }

    public function checkImageSizeOrThrowException($imagePath) {
        if (!$this->imageSizeIsAllowed($imagePath)) {
            throw new HttpException(400, 'The image must be 320px (width) by 320px (height)');
        }
    }

    public function checkImageTypeOrThrowException($imagePath) {
        if (!$this->imageTypeIsAllowed($imagePath)) {
            throw new HttpException(400, 'The image must be JPEG, PNG or GIF.');
        }
    }

}