<?php
/**
 * Created by Jorge Sivil <jorge.sivil@gmail.com>
 */

namespace App\Controller;

use App\Document\ItemsList;
use Doctrine\ODM\MongoDB\DocumentRepository;
use Stubs\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller {

    public function __construct() {
    }

    public function index() {
        /** @var DocumentRepository $itemsListRepository */
        $itemsListRepository = $this->get('doctrine_mongodb')->getRepository('App:ItemsList');
        $itemsLists = $itemsListRepository->findBy([], null, 1);

        /** @var ItemsList|null $itemsList */
        $itemsList = isset($itemsLists[0]) ? $itemsLists[0] : null;
        if (!$itemsList) {
            $itemsList = new ItemsList();
            /** @var DocumentManager $dm */
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($itemsList);
            $dm->flush();
        }

        $itemsQuantity = $itemsList === null ? 0 : sizeof($itemsList->getItems());
        return $this->render('main/index.html.twig', [
            'itemsList' => $itemsList,
            'itemsQuantity' => $itemsQuantity
        ]);
    }

}