$(function() {
    var $itemsListSelector = $("#itemslist");
    var itemsListId = $itemsListSelector.data('id');
    var $counter = $('#counter');

    // Enable dismissal of Boostrap Alerts
    $('.alert').alert();

    $itemsListSelector.sortable({
        update: function() {
            var sortedIDs = $itemsListSelector.sortable('toArray').join(',');
            var sortableThis = this;
            $.ajax({
                url: '/itemslist/'+itemsListId+'/reorder-items',
                method: 'POST',
                data: 'order='+sortedIDs,
                success: function(data, textStatus, jqXHR) {
                    console.log('Success', data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log('Error', jqXHR, textStatus, errorThrown);
                    sortableThis.sortable('cancel');
                }
            });
        }
    });

    $(document).on('click', 'button[data-btn-type="delete"]', function() {
        $this = $(this);
        $item = $this.closest('li');
        $this.prop('disabled', true);

        $.ajax({
            url: '/itemslist/'+itemsListId+'/item/'+$item.attr('id'),
            method: 'DELETE',
            success: function() {
                decrementCounter();
                $item.slideUp(400, function() {
                    $item.remove();
                });
            },
            error: function() {
                createDangerAlert('<strong>Ups!</strong> There was a problem trying to delete the item. '+
                    'Please try again or refresh the page.').appendTo($item).slideDown('slow');
            },
            complete: function() {
                $this.prop('disabled', false);
            }
        });
    });

    $(document).on('click', 'button[data-btn-type="edit"]', function() {
        var $this = $(this);
        var $item = $this.closest('li');
        var itemDescription = $item.find('p.item-description').html();
        var $modal = $('#edit-item-modal');
        if ($modal.length > 0) {
            $modal.remove();
        }
        createItemEditModal($item.prop('id'), itemDescription).appendTo($('body'));
        $('#edit-item-modal').modal('show');
    });

    $(document).on('click', '#edit-item-modal-close-btn', function() {
        var $modal = $('#edit-item-modal');
        $modal.modal('hide');
    });

    $(document).on('submit', '#form-edit-item', function(e) {
        e.preventDefault();
        var $this = $(this);
        var itemId = $this.data('id');
        var $item = $('#'+itemId);
        var formData = new FormData($this[0]);
        formData.append('outputHtml', true);

        $.ajax({
            type:'POST',
            url:'/itemslist/'+itemsListId+'/item/'+itemId,
            processData: false,
            contentType: false,
            cache: false,
            data : formData,
            success: function(data){
                $item.replaceWith(data);
                createSuccessAlert('<strong>Yay!</strong> Item was successfully updated.').appendTo($this).slideDown('slow');
                setTimeout(function() {
                    $('#edit-item-modal').modal('hide');
                }, 2000);
            },
            error: function() {
                createDangerAlert('<strong>Ups!</strong> There was a problem trying to update the item. '+
                    'Please try again or refresh the page.').appendTo($this).slideDown('slow');
            },
            complete: function() {
            }
        });
    });

    $(document).on('submit', '#form-create-new-item', function(e) {
        $button = $('#btn-add-new-item');
        $button.prop('disabled', true);
        e.preventDefault();
        $this = $(this);
        var formData = new FormData($this[0]);
        formData.append('outputHtml', true);
        $.ajax({
            type:'POST',
            url:'/itemslist/'+itemsListId+'/items',
            processData: false,
            contentType: false,
            cache: false,
            data : formData,
            success: function(data){
                $(data).appendTo($itemsListSelector).slideDown('slow');
                $this.trigger('reset');
                incrementCounter();
            },
            error: function() {
                createDangerAlert('<strong>Ups!</strong> There was a problem trying to create the item. '+
                    'Please try again or refresh the page.').appendTo($this).slideDown('slow');
            },
            complete: function() {
                $button.prop('disabled', false);
            }
        });
    });

    $(document).on('click', '.browse', function(){
        var file = $(this).parent().parent().parent().find('.file');
        file.trigger('click');
    });

    $(document).on('change', '.file', function(){
        $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
    });

    function incrementCounter() {
        var counter = parseInt($counter.html());
        $counter.html(counter+1);
    }

    function decrementCounter() {
        var counter = parseInt($counter.html());
        $counter.html(counter-1);
    }

    function createDangerAlert(message) {
        return $('<div class="alert alert-danger alert-dismissible fade show" role="alert">'+message+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span></button></div>')
    }

    function createSuccessAlert(message) {
        return $('<div class="alert alert-success alert-dismissible fade show" role="alert">'+message+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
            '<span aria-hidden="true">&times;</span></button></div>')
    }

    function createItemEditModal(itemId, itemDescription) {
        return $('<div class="modal fade" id="edit-item-modal" tabindex="-1" role="dialog" aria-hidden="true">' +
            '    <div class="modal-dialog" role="document">' +
            '        <div class="modal-content">' +
            '            <div class="modal-header">' +
            '                <h5 class="modal-title">Editing Item '+itemId+'</h5>' +
            '                <button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
            '                    <span aria-hidden="true">&times;</span>' +
            '                </button>' +
            '            </div>' +
            '            <div class="modal-body">' +
            '                <form id="form-edit-item" autocomplete="off" data-id="'+itemId+'">' +
            '                    <div class="form-group">' +
            '                        <label for="form-field-description">Description</label>' +
            '                        <input type="text" name="description" maxlength="300" id="form-field-description"' +
            '                               class="form-control" placeholder="Enter item description" value="'+itemDescription+'">' +
            '                        <small class="form-text text-muted">Up to 300 characters.</small>' +
            '                    </div>' +
            '                    <div class="input-group">' +
            '                        <input type="file" name="image" class="file">' +
            '                        <input type="text" class="form-control input-lg" disabled placeholder="Upload new image">' +
            '                        <span class="input-group-btn">' +
            '                    <button class="browse btn btn-primary input-lg" type="button">' +
            '                        <i class="far fa-folder-open"></i> Browse' +
            '                    </button>' +
            '                </span>' +
            '                    </div>' +
            '                    <small class="form-text text-muted">Must be 320px (width) by 320px (height). Only JPEG, PNG or GIF.' +
            '                    </small>' +
            '                    <div class="modal-footer">' +
            '                        <button type="button" id="edit-item-modal-close-btn" class="btn btn-secondary">Close</button>' +
            '                        <button type="submit" id="edit-item-modal-save-btn" class="btn btn-primary">Save changes</button>' +
            '                    </div>' +
            '                </form>' +
            '            </div>' +
            '        </div>' +
            '    </div>' +
            '</div>')
    }
});


